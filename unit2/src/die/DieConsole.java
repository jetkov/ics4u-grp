package die;

import java.util.Scanner;

/**
 * Simple console implementation of the Die View
 *
 * @author Ben
 */
public class DieConsole extends DieView implements Runnable {
	public DieConsole() {
		System.out.println("enter a number");

		// Starts the runnable
		new Thread(this).start();
	}

	// Runnable view, uses an infinite loop to constantly notify the controller to check the value of updateDice
	public void run() {
		while (true) {
			setChanged();
			notifyObservers();
		}
	}

    /**
     *  Uses scanner to update the value of the die sides. Overides abstract updateDice method
     *
     *  @return console input for number of sides on a die
     */
	@SuppressWarnings("resource")
	@Override
	public int getDieSides() {
		return Integer.parseInt(new Scanner(System.in).nextLine());
	}

    /**
     * Used to print last roll to the console
     *
     * @param roll value to print
     */
	@Override
	public void printRoll(int roll) {
		System.out.println("Last roll: " + roll);
	}

    /**
     * Prints an error to the console in case of an exception
     */
	public void printError() {
		System.out.println("Enter an actual number idiot");
	}
}
