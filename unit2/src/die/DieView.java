package die;

import java.util.Observable;

/**
 * @author Ben
 *
 * Abstract class for all view classes
 */
public abstract class DieView extends Observable {

	/**
	 * Method called by controller to get the current number of sides of a die
	 *
	 * @return Number of sides of a die the user wants to enter
	 */
	public abstract int getDieSides();

	/**
	 * Method to print last die roll to the view
	 *
	 * @param roll a number to print
	 */
	public abstract void printRoll(int roll);

	/**
	 * Prints error to the view in case of a handled exception
	 */
	public abstract void printError();

}
