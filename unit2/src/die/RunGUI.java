package die;

/**
 * Simple driver class to create a new instance of the application with a GUI (DieRoller)
 * 
 * @author Alex
 *
 */
public class RunGUI {
	public static void main(String[] args) {
		new DieController(new DieRoller());
	}
}
