package die;

/**
 * Runs a console version of the application
 *
 * Created by Ben on 21-Mar-17.
 */
public class RunConsole {
	public static void main(String[] args) {
		DieConsole view = new DieConsole();
		new DieController(view);
	}
}
