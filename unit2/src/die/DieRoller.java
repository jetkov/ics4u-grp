
package die;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * @author Alex Petkovic
 */

public class DieRoller extends DieView {

	// Declaring these as static even though they are used in an instance of this object
	// Class variables because some values are used in other methods
	private final static String[] DIE_TYPE = { "4", "6", "8", "10", "12", "20", "Other" };
	private final static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

	private JTextField customDieInput = new JTextField();
	private JComboBox<String> dieSelector = new JComboBox<>(DIE_TYPE);
	private JButton rollButton = new JButton("Roll!");
	private JTextArea rolls = new JTextArea();
	private JScrollPane outputRolls = new JScrollPane(rolls);

	public DieRoller() {
		createGUI();
	}

	/**
	 * Adds all the content to the window
	 *
	 * @param frame
	 *            The frame to set up
	 */
	public void setupFrame(JFrame frame) {

		// All the labels
		JLabel titleLbl = new JLabel("Virtual Die Roller");
		titleLbl.setFont(titleLbl.getFont().deriveFont(18f));
		titleLbl.setBounds(40, 15, 200, 20); // x axis, y axis, width, height

		JLabel comboxLbl = new JLabel("Number of Sides: ", SwingConstants.RIGHT);
		comboxLbl.setBounds(20, 60, 100, 10);

		JLabel customLbl = new JLabel("Custom: ", SwingConstants.RIGHT);
		customLbl.setBounds(20, 85, 100, 10);

		// Setting up the layout
		dieSelector.setBounds(125, 55, 80, 20);
		customDieInput.setBounds(125, 80, 81, 21);
		rollButton.setBounds(50, 120, 120, 30);
		outputRolls.setBounds(20, 170, 185, 70);

		// Sets the default die to 6
		dieSelector.setSelectedIndex(1);

		// Enables custom dice input if the 'Other' box is selected
		customDieInput.setEnabled(false);
		dieSelector.addActionListener(e -> { // Lambda!
			if (String.valueOf(dieSelector.getSelectedItem()).equals("Other")) {
				customDieInput.setEnabled(true);
			} else {
				customDieInput.setEnabled(false);
			}
		});

		rollButton.addActionListener(e -> {
			setChanged();
			notifyObservers();
		});

		// Adding the labels
		frame.add(titleLbl);
		frame.add(comboxLbl);
		frame.add(customLbl);

		// Adding interactive items
		frame.add(dieSelector);
		frame.add(customDieInput);
		frame.add(rollButton);
		frame.add(outputRolls);

	}

	/**
	 * Sets up the frame, and fills it using the setup method
	 */
	public void createGUI() {

		// The actual window
		JFrame frame = new JFrame("Die Roller");
		frame.setLayout(null);
		frame.setSize(230, 290);
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		// Create and set up the content pane.
		setupFrame(frame);

		// Display the window (in the center of the screen)
		frame.setLocation(screenSize.width / 2 - frame.getSize().width / 2,
				screenSize.height / 2 - frame.getSize().height / 2);

		frame.setResizable(false);
		frame.setVisible(true);
	}

	/**
	 * Returns the selected content of the die selector
	 */
	public int getDieSides() {
		if (customDieInput.isEnabled()) {
			return Integer.parseInt(customDieInput.getText());
		}
		return Integer.parseInt(String.valueOf(dieSelector.getSelectedItem()));
	}

	/**
	 * Appends a roll value to the output box
	 */
	public void printRoll(int roll) {
		rolls.append(roll + "\n");
	}

	/**
	 * Puts an error in the output box if invalid entry
	 */
	public void printError() {
		rolls.append("Enter an actual number \n");
	}

}
