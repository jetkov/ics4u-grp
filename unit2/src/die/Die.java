package die;

/**
 * The die object, with a number of sides and the ability to be rolled and generate a
 * random number.
 *
 * It is the 'model' in the model-view-controller pattern used in this application.
 *
 * @author Ben Plante
 *
 */
public class Die {
	private int sides;

    /**
     * Constructs a die object with n sides. It can be rolled to generate a random number in a range.
     *
     * @param sides Number of sides for this die
     */
	public Die(int sides) {
		this.sides = sides;
	}

    /**
     * 'Rolls' the die to generate a random number.
     *
     * @return a randomly generated integer in the range of the number of faces of the die
     */
	public int roll() {
	    // casts double value of Math.Random to an integer and adds 1 since casting will always round down.
		return (int) (Math.random() * sides + 1);
	}

    /**
     * Get the number of faces on the die
     *
     * @return integer number of the faces
     */
	public int getSides() {
		return sides;
	}

    /**
     * changes the sides of the die
     *
     * @param sides new value for number of faces
     */
	public void setSides(int sides) {
		this.sides = sides;
	}
}
