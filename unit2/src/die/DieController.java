package die;

/**
 * Manages the die and the view, creating a die with a specific number of sides given by
 * the view, rolling it, and passing the roll value back to to the view.
 *
 * It is the 'controller' in the model-view-controller pattern used in this application.
 *
 * @author Ben Plante
 */
public class DieController {

	/**
	 * Creates a new die controller
	 *
	 * @param view
	 *            The DieView 'view' to control
	 */
	public DieController(DieView view) {

		// Adds an anonymous observer object to the view
		view.addObserver((o, arg) -> {

			// Tries to create a new die with an integer number of sides
			// and print the random roll value to the view
			try {
				Die die = new Die(view.getDieSides());
				view.printRoll(die.roll());
			}

			// Prints an error to the view output if the entry is invalid
			catch (NumberFormatException ex) {
				view.printError();
			}

		});
	}
}
