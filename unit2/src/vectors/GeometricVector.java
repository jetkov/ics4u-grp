package vectors;

import java.util.Arrays;

/**
 * An extension of Vector with Geometric vector specific methods and constructors
 * 
 * @author Alex Petkovic & Ben Plante
 */
public class GeometricVector extends Vector {
	private double magnitude;
	private double[] angles;

	/**
	 * Creates a new vector
	 */
	public GeometricVector() {
		super();
		magnitude = 0;
		angles = null;
	}

	/**
	 * Creates a new vector
	 * 
	 * @param magnitude
	 *            - The magnitude of the vector
	 * @param angles
	 *            - A list of angles (in radians) from the principle axes. The size of the
	 *            list determines the dimension of the vector.
	 */
	public GeometricVector(double magnitude, double... angles) {
		super();

		this.magnitude = magnitude;
		this.angles = angles;

		setComponents(toComponents());
	}

	public double getMagnitude() {
		return magnitude;
	}

	public void setMagnitude(double magnitude) {
		this.magnitude = magnitude;
	}

	public double[] getAngles() {
		return angles;
	}

	public void setAngles(double[] angles) {
		this.angles = angles;
	}

	/**
	 * Calculates the components of this vector
	 * 
	 * @return A double array of components
	 */
	private double[] toComponents() {
		double[] components = new double[angles.length];

		for (int i = 0; i < angles.length; i++) {
			components[i] = Math.cos(angles[i]);
		}

		return components;
	}

	/**
	 * Multiplies the magnitude instead of the components, and then converts the geometric
	 * vector to components again and sets the super's components to these.
	 * 
	 * @param multiple
	 *            - The scalar to multiply by
	 */
	@Override
	public void scalarMultiplyBy(double multiple) {
		magnitude *= multiple;
		setComponents(toComponents());
	}

//	public CartesianVector toCartesian() {
//		return new CartesianVector(super.getComponents());
//	}
	
	/**
	 * @return The magnitude of the vector and the list of its angles
	 */
	public String toString() {
		return "Magnitude: " + magnitude + "\nAngles: " + Arrays.toString(angles);
	}

	/**
	 * @return List of the components of the vector as a String
	 */
	public String componentsToString() {
		return Arrays.toString(getComponents());
	}

}
