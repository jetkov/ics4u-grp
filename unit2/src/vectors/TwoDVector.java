package vectors;

/**
 * A specialized 2D version of the Cartesian vector
 * 
 * @author Alex Petkovic & Ben Plante
 *
 */
public class TwoDVector extends CartesianVector {
private double x, y;
	
	public TwoDVector() {
		super();
	}

	public TwoDVector(double x, double y) {
		super(new double[]{x, y});
		
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return super.getComponents()[0];
	}
	
	public double getY() {
		return super.getComponents()[1];
	}

	public void setX(double x) {
		this.x = x;
		super.setComponents(new double[]{x, y});
	}

	public void setY(double y) {
		this.y = y;
		super.setComponents(new double[]{x, y});
	}

}
