package vectors;

/**
 * An extension of Vector with Cartesian specific methods
 * 
 * @author Alex Petkovic & Ben Plante
 */
public class CartesianVector extends Vector {
	private double[] components;

	/**
	 * Creates a new vector
	 */
	public CartesianVector() {
		super();
		components = null;
	}

	/**
	 * Creates a new vector
	 * 
	 * @param comonents
	 *            - A list of vector components. The number of components defines the
	 *            vectors dimension.
	 */
	public CartesianVector(double... components) {
		super(components);
		this.components = components;
	}

	/**
	 * Multiples this vector by a scalar value
	 * 
	 * @param multiple
	 *            - The scalar to multiply by
	 */
	public void scalarMultiplyBy(double multiple) {
		for (int i = 0; i < components.length; i++) {
			components[i] *= multiple;
		}
		super.setComponents(components);
	}

//	public GeometricVector toGeometric() {
//		double[] angles = new double[components.length];
//		double magnitude = getMagnitude();
//
//		for (int i = 0; i < components.length; i++) {
//			angles[i] = Math.acos(components[i] / magnitude);
//		}
//
//		return new GeometricVector(magnitude, angles);
//	}

}
