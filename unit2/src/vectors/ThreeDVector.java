package vectors;

/**
 * A specialized 3D version of the Cartesian vector
 * 
 * @author Alex Petkovic & Ben Plante
 *
 */
public class ThreeDVector extends CartesianVector {
	private double x, y, z;
	
	public ThreeDVector() {
		super();
	}

	public ThreeDVector(double x, double y, double z) {
		super(new double[]{x, y, z});
		
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return super.getComponents()[0];
	}
	
	public double getY() {
		return super.getComponents()[1];
	}
	
	public double getZ() {
		return super.getComponents()[2];
	}

	public void setX(double x) {
		this.x = x;
		super.setComponents(new double[]{x, y, z});
	}

	public void setY(double y) {
		this.y = y;
		super.setComponents(new double[]{x, y, z});
	}

	public void setZ(double z) {
		this.z = z;
		super.setComponents(new double[]{x, y, z});
	}
	
	
}
