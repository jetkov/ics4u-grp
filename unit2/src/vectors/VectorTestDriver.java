package vectors;

/**
 * Tests the functionality of the vector classes
 */
public class VectorTestDriver {
	public static void main(String[] args) {
		CartesianVector carV1 = new CartesianVector(1, 5, 7);
		CartesianVector carV2 = new CartesianVector(-1, -4, 3);  // perpendicular
		
		GeometricVector geoV1 = new GeometricVector(43, 0.3, 0.5, 1.0);
		GeometricVector geoV2 = new GeometricVector(34, 0.7, 1.3, 0.4);
		
		TwoDVector twoDV = new TwoDVector(2,5);
		ThreeDVector threeDV = new ThreeDVector(5,7,23);
		
		carV1.scalarMultiplyBy(-1);
		geoV1.scalarMultiplyBy(-2);
		
		carV1.invert();
		geoV2.invert();
		
		System.out.println("- Car V1 - ");
		System.out.println(carV1 + "\n");
		System.out.println("- Car V2 - ");
		System.out.println(carV2 + "\n");
		
		System.out.println("- Geo V1 - ");
		System.out.println(geoV1 + "\n");
		System.out.println("- Geo V2 - ");
		System.out.println(geoV2 + "\n");
		
		System.out.println("Geo V1 Components:   " + geoV1.componentsToString());
		System.out.println("Geo V2 Components:   " + geoV2.componentsToString() + "\n");
		
		System.out.println("Sum Car V1 + Geo V1:           " + Vector.sum(carV1, geoV1));
		System.out.println("Dot Product Car V1 * Geo V1:   " + Vector.dotProduct(carV1, geoV1) + "\n");
		
		System.out.println("Sum Car V1 + Car V2:           " + Vector.sum(carV1, carV2));
		System.out.println("Dot Product Car V1 * Car V2:   " + Vector.dotProduct(carV1, carV2) + "\n");
		
		System.out.println("Sum Geo V1 + Geo V2:           " + Vector.sum(geoV1, geoV2));
		System.out.println("Dot Product Geo V1 * Geo V2:   " + Vector.dotProduct(geoV1, geoV2) + "\n");		
		
		System.out.println("Sum 2DV + 2DV:           " + twoDV.sumWith(twoDV));
		System.out.println("Dot Product 2DV * 2DV:   " + twoDV.dotProductWith(twoDV)  + "\n");
		
		// Supposed to get an exception here
		System.out.println("Sum 2DV + 3DV:           " + twoDV.sumWith(threeDV));
		System.out.println("Dot Product 2DV * 3DV:   " + twoDV.dotProductWith(threeDV));
		
//		System.out.println("Car V1 -> GeoVector -> CarVector: ");
//		System.out.println(carV1.toGeometric().toCartesian());
	}
}
