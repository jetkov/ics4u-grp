package vectors;

import java.util.Arrays;

/**
 * This is an abstract class that serves as a vector superclass to different types of
 * vectors of infinite dimensions.
 * 
 * @author Alex Petkovic & Ben Plante
 */
public abstract class Vector {
	private double[] components;

	/**
	 * Creates a new vector
	 */
	public Vector() {
		components = null;
	}

	/**
	 * Creates a new vector
	 * 
	 * @param comonents
	 *            - A list of vector components. The number of components defines the
	 *            vectors dimension.
	 */
	public Vector(double... components) {
		this.components = components;
	}

	public double[] getComponents() {
		return components;
	}

	public void setComponents(double components[]) {
		this.components = components;
	}

	public double getMagnitude() {
		double sumOfSquares = 0;

		for (double component : components) {
			sumOfSquares += component * component; // Thanks Pythagoras for infinite
													// dimension support
		}

		return Math.sqrt(sumOfSquares);
	}

	/**
	 * Sums this vector with another vector of the same dimension
	 * 
	 * @param otherVector
	 *            - Vector to sum with this one
	 * @return The resultant vector
	 */
	public Vector sumWith(Vector otherVector) {
		int dimension = components.length;

		double otherComponents[] = otherVector.getComponents();
		double newComponents[] = new double[dimension];

		if (dimension == otherComponents.length) {
			for (int i = 0; i < dimension; i++) {
				newComponents[i] = components[i] + otherComponents[i];
			}
			return new CartesianVector(newComponents);
		}

		throw new IllegalArgumentException("ERROR: Dimensional mismatch!");
	}

	public static Vector sum(Vector v1, Vector v2) {
		return v1.sumWith(v2);
	}

	/**
	 * Caclculates the dot product of this vector and another vector of the same
	 * dimension.
	 * 
	 * @param otherVector
	 *            - Vector to dot product with this one
	 * @return The resultant scalar
	 */
	public double dotProductWith(Vector otherVector) {
		int dimension = components.length;

		double otherComponents[] = otherVector.getComponents();
		double dotProduct = 0;

		if (dimension == otherComponents.length) {
			for (int i = 0; i < dimension; i++) {
				dotProduct += components[i] * otherComponents[i];
			}
			return dotProduct;
		}

		throw new IllegalArgumentException("ERROR: Dimensional mismatch!");
	}

	public static double dotProduct(Vector v1, Vector v2) {
		return v1.dotProductWith(v2);
	}

	public void invert() {
		for (int i = 0; i < components.length; i++) {
			components[i] *= -1;
		}
	}

	public abstract void scalarMultiplyBy(double multiple);

	/**
	 * @return List of the components of the vector as a String
	 */
	public String toString() {
		return Arrays.toString(components);
	}

}
